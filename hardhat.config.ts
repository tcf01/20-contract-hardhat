import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";

dotenv.config();

const { ALCHEMY_API_URL, PRIVATE_KEY } = process.env;


task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

const config: HardhatUserConfig = {
  solidity: "0.8.13",
  defaultNetwork: "rinkeby",
  networks: {
    rinkeby: {
      url: ALCHEMY_API_URL,
      accounts: [`0x${PRIVATE_KEY}`], //who you are in the blockchain world
      gas: 2100000,
      gasPrice: 8000000000,
    }
  },
  /* gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },*/
  etherscan: {
    apiKey: process.env.ETHERSCAN_API_KEY,
  }, 
};

export default config;
