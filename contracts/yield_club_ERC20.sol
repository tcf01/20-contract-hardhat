// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

interface YieldOriginal{
    function balanceOf(address _targetAddress, uint256 id) external view returns(uint256);
}

contract YieldToken is ERC20PresetMinterPauser("Yield", "CROP"), Ownable{
    uint256 public duration = 1 minutes;
    uint256 public emissionPerDuration = 10;

    uint256 public immutable emissionStart;
    uint256 public immutable emissionEnd;

    mapping(address => uint256) private addressLastClaimTimestamp; 
    mapping(uint256 => uint256) private _lastClaim;

    address private _relatedContractAddress;

   
    constructor(uint256 emissionStartTimestamp) {
        emissionStart = emissionStartTimestamp;
        emissionEnd = emissionStartTimestamp + (1 days * 365 * 300);
    }

    // External functions
    function setRelatedContractAddress(address contractAddress) external onlyRole(DEFAULT_ADMIN_ROLE) {
        // NOTE: for dev, temp comment this out
        // require(_relatedContractAddress == address(0), "Already set");

        _relatedContractAddress = contractAddress;
    }


    function modifyEmissionPerDuration(uint256 newEmissionDuration) external onlyOwner{
        emissionPerDuration = newEmissionDuration;
    }

    function checkCurrRelatedContractAddress() public view onlyOwner returns(address){
        return _relatedContractAddress;
    }

    //NOTE: need to add the burn token function for future usage!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    // Public functions
    function changeDuration(uint256 newDuration) external {
        duration = newDuration;
    }

    function getAddressLastClaim(address targetAddress) public view returns(uint256){
        return addressLastClaimTimestamp[targetAddress];
    }

    function testingClaimToken(address _receipent) external{
        require(_relatedContractAddress != address(0), "Error Code: C001. Contact us please");  //please set related the contract address first  
        require(block.timestamp < emissionEnd, "Claim token period has ended");

        bool isFirstTimeClaim = addressLastClaimTimestamp[msg.sender] == 0;

        uint256 lastClaimTimeInSecond = isFirstTimeClaim ? emissionStart : addressLastClaimTimestamp[msg.sender];
        require (isFirstTimeClaim || block.timestamp - lastClaimTimeInSecond > duration, "Cooldown period now...");

        uint256 zeroTokenAmount = YieldOriginal(_relatedContractAddress).balanceOf(_receipent,0);
        require(zeroTokenAmount > 0, "only XXX owner can claim token");

        uint256 timePassSinceLastClaim = (block.timestamp - lastClaimTimeInSecond) / duration;
        uint256 accumulatedToken = timePassSinceLastClaim * emissionPerDuration;
       
        _mint(msg.sender, accumulatedToken * 10 ** uint(decimals()));
        
        addressLastClaimTimestamp[msg.sender] = block.timestamp;
    }
}