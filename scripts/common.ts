import { run } from 'hardhat';


export async function verifyContract(contractAddress: string, param: any[] = []) {
    await run("verify:verify", {
        address: contractAddress,
        constructorArguments: param,
    });
}


