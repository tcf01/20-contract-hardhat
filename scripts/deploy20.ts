import { ethers } from "hardhat";
import { yieldClubContract20 } from "../constants";
import "@nomiclabs/hardhat-etherscan";
import { verifyContract } from "./common";

const tenSecondAgo = Date.now() - 10000; 

async function main() {
  const YieldClub20 = await ethers.getContractFactory(yieldClubContract20)
  const YieldClub20Contract = await YieldClub20.deploy(tenSecondAgo);

  console.log("ERC20 Contract deployed to address: ", YieldClub20Contract.address);
}



main()
  .then(() => { console.log("all the process has been done") })
  .catch((error) => { console.error(error); process.exitCode = 1 });
/* 
verifyContract("0x63e826A96C947295324e6983E0eCc077Dfb72bDC", [tenSecondAgo])
  .then(() => { console.log('all process has been done') })
  .catch((error) => console.error(error)) */