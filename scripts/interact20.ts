import { ethers } from "hardhat";
import moment from 'moment-timezone'
import { yieldClubContract20, targetNetwork } from "../constants";


const ALCHEMY_API_KEY = process.env.ALCHEMY_API_KEY;
const PRIVATE_KEY = process.env.PRIVATE_KEY;
const CONTRACT_20_ADDRESS = process.env.CONTRACT_20_ADDRESS;

// start developing
const contract = require(`../artifacts/contracts/yield_club_ERC20.sol/${yieldClubContract20}.json`);

const alchemyProvider = new ethers.providers.AlchemyProvider(targetNetwork, ALCHEMY_API_KEY);
const signer = new ethers.Wallet(PRIVATE_KEY!!, alchemyProvider); // Signer
const c20 = new ethers.Contract(CONTRACT_20_ADDRESS!!, contract.abi, signer);


const setInitConfig = async () => {
    try {
        const res = await c20.setRelatedContractAddress(process.env.CONTRACT_1155_ADDRESS);

        console.log('all done')
    } catch (e) {
        console.error(e)
    }
}

const claimToken = async () => {
    try {
        const res = await c20.testingClaimToken("0x4643Dd1c6B8312f0E0a7F26971C09530aCc80988")

        console.log(res)
        console.log("all done")

    } catch (error) {
        console.log(error)
    }
}



setInitConfig()